import { Component, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { MatSelectChange, MatSnackBar } from '@angular/material';

import { SnackBarComponent } from '../snack-bar/snack-bar.component';

const enum ContactType {
  Email,
  Phone
}

@Component({
  selector: 'app-ang-form',
  templateUrl: './ang-form.component.html'
})
export class AngFormComponent implements OnInit {
  readonly contactTypes = [
    {
      text: 'email',
      type: ContactType.Email
    },
    {
      text: 'phone',
      type: ContactType.Phone
    },
  ];
  userForm: FormGroup;
  // @ts-ignore
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(private formBuilder: FormBuilder, public snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      userName: new FormControl(null, Validators.required),
      companyName: new FormControl(null),
      userInfoArray: new FormArray([
        this.createContactGroup()
      ])
    });
  }

  onSaveUserProfile(): void {
    console.log('Form Result::', this.userForm.controls);
    if (!this.userForm.valid) {
      return;
    }
    const userName = this.userForm.get('userName').value;
    const snackBarRef = this.snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      panelClass: ['app-snackbar']
    });
    snackBarRef.instance.message = 'User saved successfully';
    setTimeout(() => {
      this.formGroupDirective.resetForm();
    }, 0);
  }

  createContactGroup() {
    return new FormGroup({
      contactType: new FormControl(this.contactTypes[0].type ),
      contactLabel: new FormControl(null),
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }

  onAddContacts(): void {
    (this.userForm.get('userInfoArray') as FormArray).push(this.createContactGroup());
    setTimeout(() => {
      this.userForm.markAsUntouched();
    }, 0);
  }

  getUserInfoGroup() {
    return (this.userForm.get('userInfoArray') as FormArray).controls;
  }

  switchField({value}: MatSelectChange, index: number): void {
    const group = (this.userForm.get('userInfoArray') as FormArray).get(index.toString()) as FormGroup;

    if (value === ContactType.Email) {
      group.removeControl('phone');
      group.addControl('email', new FormControl(null, [Validators.required, Validators.email]));
    } else {
      group.removeControl('email');
      group.addControl('phone', new FormControl(null, [Validators.required, this.phoneNumberValidator]));
    }
  }

  onRemoveContact(index: number): void {
    (this.userForm.get('userInfoArray') as FormArray).removeAt(index);
    setTimeout(() => {
      this.userForm.markAsUntouched();
    }, 0);
  }

  phoneNumberValidator = (
    control: AbstractControl
  ): ValidationErrors | null => {
    const valid = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g.test(control.value) && control.value.length > 6;
    return valid
      ? null
      : { invalidNumber: true };
  }
}
